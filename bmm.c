#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <unistd.h>

#define XSIZE 400
#define YSIZE 400
#define VASIZE 48
#define BSIZE 1


typedef struct cacheInfo{
  int numBlocks; // number of blocks
  int wordsPerBlock; //words per block
  int bitsTag; //number of bits of tag
  int bitsIndex; //number of bits of index
  int bitsOffset; //bits of offset (log2(NumBlocks))

} cacheInfo;

const int offset32 = 0x1FFF; //use this as the filter for the index and tag
const int offset128 = 0x7FFF; //use this as the filter for the index and tag

int main(){

  cacheInfo cache32Info = {1024,8,0,10,3};
  cache32Info.bitsTag = VASIZE - cache32Info.bitsIndex - cache32Info.bitsOffset;
  cacheInfo cache128Info = {2048,16,0,11,4};
  cache128Info.bitsTag = VASIZE - cache128Info.bitsIndex - cache128Info.bitsOffset;
  int x[XSIZE][YSIZE];
  int y[XSIZE][YSIZE];
  int z[XSIZE][YSIZE]; //note that z is the lowest address, so we will assume that its address is the mask; we will be subtracting z from every address to geth the data


  //create three arrays to simulate the 32K cache
  int cache32Data[cache32Info.numBlocks][cache32Info.wordsPerBlock]; //This 2-D array is for the data
  int cache32Valid[cache32Info.numBlocks]; //This 1-D array is for the valid bits
  for(int i=0; i<cache32Info.numBlocks; i++){
    cache32Valid[i] = 0;
  }
  long cache32Tags[cache32Info.numBlocks]; //This holds the tags
  for(int i=0; i<cache32Info.numBlocks; i++){
    cache32Tags[i] = -1;
  }



  //create three arrays to simulate the 128K cache
  int cache128Data[cache128Info.numBlocks][cache128Info.wordsPerBlock]; //This 2-D array is for the data
  int cache128Valid[cache128Info.numBlocks]; //This 1-D array is for the valid bits
  for(int i = 0; i<cache128Info.numBlocks; i++){
    cache128Valid[i] = 0;
  }
  long cache128Tags[cache128Info.numBlocks]; //This holds the tags
  for(int i=0; i<cache128Info.numBlocks; i++){
    cache128Tags[i] = -1;
  }


  int i, j, k, jj, kk, jmin, kmin;

  int r;
  printf("X: %p\n",x);
  printf("Y: %p\n",y);
  printf("Z: %p\n",z);
  /* Initialize x matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      x[i][j] = 0;
    }
  }

  /* Initialize y matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
      y[i][j] = i + j;
    }
  }

  /* Initialize z matrix */
  for(i=0; i<XSIZE; i++) {
    for(j=0; j<YSIZE; j++) {
        z[i][j] = i + j;
    }
  }

  long misses32 = 0;
  long hits32 = 0;
  long hits128 = 0;
  long misses128 = 0;

  /* Matrix Multiply */
  for (jj=0; jj<XSIZE; jj+=BSIZE) {
    for (kk=0; kk<XSIZE; kk+=BSIZE) {
      for (i=0; i<XSIZE; i++) {
        if ((jj+BSIZE) < XSIZE){
          jmin = jj+BSIZE;
        }
        else{
          jmin = XSIZE;
        }
        for (j=jj; j < jmin; j++) {
          r = 0;
          if ((kk+BSIZE) < XSIZE){
            kmin = kk+BSIZE;
          }
          else{
            kmin = XSIZE;
          }
          for (k=kk; k < kmin; k++) {
            r = r + y[i][k] * z[k][j];


        //update the cache
        intptr_t yp = y[i] + k*sizeof(int);
        intptr_t  zp = z[k] + j*sizeof(int);

        long cache32TagY = yp >> (cache32Info.bitsIndex + cache32Info.bitsOffset);
        int cache32IndexY = (yp & offset32) >> cache32Info.bitsOffset;
        int cache32OffsetY = (yp & 7);
        //cache32TagZ = zp >> (cache32Info.bitsInfo + cache32Info.bitsOffset);
        //update the 32Kbyte cache
        //first, fill y
        if(cache32Tags[cache32IndexY % cache32Info.numBlocks] == cache32TagY){ //check if the tag is in the spot. If it is, check if it is valid.
                if(cache32Valid[cache32IndexY % cache32Info.numBlocks]){
                        hits32++; //if valid and present, increment hits
                } else{
                        misses32++; //it  is a cache miss, the data would be reloaded into the cache
                        cache32Valid[cache32IndexY %  cache32Info.numBlocks ] = 1;
                        cache32Tags[cache32IndexY %  cache32Info.numBlocks] = cache32TagY;
                }
        } else{
                misses32++;
                cache32Valid[cache32IndexY %  cache32Info.numBlocks] = 1;
                cache32Tags[cache32IndexY %  cache32Info.numBlocks] = cache32TagY;
        }



        //now, fill with z
        long cache32TagZ = zp >> (cache32Info.bitsIndex + cache32Info.bitsOffset);
        int cache32IndexZ = (zp & offset32) >> cache32Info.bitsOffset;
        int cache32OffsetZ = (zp & 7);
         if(cache32Tags[cache32IndexY % cache32Info.numBlocks] == cache32TagZ){ //check if the tag is in the spot. If it is, check if it is valid.
                if(cache32Valid[cache32IndexZ % cache32Info.numBlocks]){
                        hits32++; //if valid and present, increment hits
                } else{
                        misses32++; //it  is a cache miss, the data would be reloaded into the cache
                        cache32Valid[cache32IndexZ %  cache32Info.numBlocks ] = 1;
                        cache32Tags[cache32IndexZ %  cache32Info.numBlocks] = cache32TagZ;
                }
        } else{
                misses32++;
                cache32Valid[cache32IndexZ %  cache32Info.numBlocks] = 1;
                cache32Tags[cache32IndexZ %  cache32Info.numBlocks] = cache32TagZ;
        }

        //Now, update the 128 kb caches


        //Update the Y

        long cache128TagY = yp >> (cache128Info.bitsIndex + cache128Info.bitsOffset);
        int cache128IndexY = (yp & offset128) >> cache128Info.bitsOffset;
        int cache128OffsetY = (yp & 15);


        if(cache128Tags[cache128IndexY % cache128Info.numBlocks] == cache128TagY){ //check if the tag is in the spot. If it is, check if it is valid.
                if(cache128Valid[cache128IndexY % cache128Info.numBlocks]){
                        hits128++; //if valid and present, increment hits
                } else{
                        misses128++; //it  is a cache miss, the data would be reloaded into the cache
                        cache128Valid[cache128IndexY %  cache128Info.numBlocks ] = 1;
                        cache128Tags[cache128IndexY %  cache128Info.numBlocks] = cache128TagY;
                }
        } else{
                misses128++;
                cache128Valid[cache128IndexY %  cache128Info.numBlocks] = 1;
                cache128Tags[cache128IndexY %  cache128Info.numBlocks] = cache128TagY;
        }



        //Update the Z
        long cache128TagZ = zp >> (cache128Info.bitsIndex + cache128Info.bitsOffset);
        int cache128IndexZ = (zp & offset128) >> cache128Info.bitsOffset;
        int cache128OffsetZ = (zp & 15);
         if(cache128Tags[cache128IndexZ % cache128Info.numBlocks] == cache128TagZ){ //check if the tag is in the spot. If it is, check if it is valid.
                if(cache128Valid[cache128IndexZ % cache128Info.numBlocks]){
                        hits128++; //if valid and present, increment hits
                } else{
                        misses128++; //it  is a cache miss, the data would be reloaded into the cache
                        cache128Valid[cache128IndexZ %  cache128Info.numBlocks ] = 1;
                        cache128Tags[cache128IndexZ %  cache128Info.numBlocks] = cache128TagZ;
                }
        } else{
                misses128++;
                cache128Valid[cache128IndexZ %  cache128Info.numBlocks] = 1;
                cache128Tags[cache128IndexZ %  cache128Info.numBlocks] = cache128TagZ;
        }





        //printf("%d %d \n",cache32IndexY,cache32IndexZ);
        //update the 128Kbyte cache

        //printf("%p\n",(z + k));
      //  usleep( (useconds_t) 100000);
      }
      //usleep(10000);
      x[i][j] = r;
    }
  }

  printf("32Kb:  %d misses and %d hits \n",misses32, hits32);
  printf("128Kb: %d misses and %d hits \n",misses128,hits128);


  return 0;
}}}
